[![](https://framasoft.org/nav/img/icons/ati/sites/git.png)](https://framagit.org)

🇬🇧 **Framasoft uses GitLab** for the development of its free softwares. Our Github repositories are only mirrors.
If you want to work with us, **fork us on [framagit.org](https://framagit.org)**. (no registration needed, you can sign in with your Github account)

🇫🇷 **Framasoft utilise GitLab** pour le développement de ses logiciels libres. Nos dépôts Github ne sont que des miroirs.
Si vous souhaitez travailler avec nous, **forkez-nous sur [framagit.org](https://framagit.org)**. (l'inscription n'est pas nécessaire, vous pouvez vous connecter avec votre compte Github)
* * *

Site de présentation du projet Framasoft :
# Dégooglisons Internet

Internet, c'était mieux avant. Aujourd'hui, toutes nos données sont centralisées dans de gros silos, appartenant à des entreprises américaines qui les exploitent sans scrupule.
**L'association Framasoft se lance un défi : vous proposer gratuitement un service Libre équivalent à chacun des services privateurs que vous trouvez sur Internet.**

[En savoir plus sur l'association Framasoft](https://framasoft.org)

Ce répertoire contient le code du site web présentant ce projet.
http://degooglisons-internet.org/
